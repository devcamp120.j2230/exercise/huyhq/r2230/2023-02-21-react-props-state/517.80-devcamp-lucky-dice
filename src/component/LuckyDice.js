
const { Component } = require("react");
class LuckyDice extends Component{
    constructor(props){
        super(props);

        this.state = {
            dice: 1
        }
    }
    getRandomDice = ()=>{
        this.setState({
            dice: Math.floor((Math.random() * 6) + 1) 
        })
    }
    render(){
        return(
            <>
                <button onClick={this.getRandomDice}>Get Dice</button>
                <p>Dice: {this.state.dice}</p>
                <img src={require("../asset/images/"+this.state.dice+".png")}/>
            </>
        )
    }
}

export default LuckyDice